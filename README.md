# MAX PEAK OF USED RAM
Print the actual RAM and his max peak. Also save in the var MAXRAMH the max peak of used RAM.

# What it do/Cosa fa
**[ENG]**
The script print the actual RAM usage and the max peak RAM used to now. Also it generate a variable called MAXRAMH that contain the peak maximum of ram usage (displayed in GB)

**[ITA]**
Lo script stampa l'utilizzo effettivo della RAM e il picco massimo di RAM utilizzato fino ad ora. Inoltre genera una variabile chiamata MAXRAMH che contiene il picco massimo di utilizzo RAM (visualizzato in GB)

## Info
This script need [free](https://www.geeksforgeeks.org/free-command-linux-examples/) installed

## Usage
    ##Download it
        wget https://gitlab.com/Nessugo/max-peak-of-used-ram/-/blob/main/maxram.sh
    ##make it executable
        chmod +x maxram.sh
    ##launch it
        ./maxram.sh

## Screenshot
![screenshot of script](info/screenshot.png)

## Roadmap
Can be improved

## License
[Unlicense](info/LICENSE) 
Free source project: use and share it free
