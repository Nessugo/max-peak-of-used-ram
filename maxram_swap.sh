#!/bin/bash

#inizializzo MAXRAM e MAX SWP con attuale RAM e Swap in uso (MAXRAM/SWP<-integer, MAXRAMH/SWPH<-in GB)
MAXRAM=$(free | grep Mem | awk '{print $3}')
MAXRAMH=$(free -h | grep Mem | awk '{print $3}')
MAXSWP=$(free | grep Swap | awk '{print $3}')
MAXSWPH=$(free -h | grep Swap | awk '{print $3}')

echo
echo "                  |MAX PEAK OF USED RAM|                 "
echo "|Save in the var called MAXRAMH the max peak of used RAM|"
echo "     Print the usage of RAM and Swap every 10 seconds    "
echo
echo "Press <CTRL+C> to exit."
echo

while :
do
    
  #attuale RAM in uso (TMPRAM<-integer, TMPRAMH<-in GB)
  TMPRAM=$(free | grep Mem | awk '{print $3}')
  TMPRAMH=$(free -h | grep Mem | awk '{print $3}')
  TMPSWP=$(free | grep Swap | awk '{print $3}')
  TMPSWPH=$(free -h | grep Swap | awk '{print $3}')

      #controllo quale valore è maggiore e lo lascio/metto nella VAR MAXRAM/H
      if (( TMPRAM > MAXRAM )) 
    	then
    	MAXRAM=$TMPRAM
    	MAXRAMH=$TMPRAMH
      fi

      #controllo quale valore è maggiore e lo lascio/metto nella VAR MAXRSWP/H
      if (( TMPSWP > MAXSWP )) 
    	then
    	MAXSWP=$TMPSWP
    	MAXSWPH=$TMPSWPH
      fi

  #stampo memoria attuale e memoria di picco in Gb
  echo The actual usage of ram is "| $TMPRAMH |"
  echo The max used RAM to now is "| $MAXRAMH |"
  echo
  echo The actual usage of Swap is "| $TMPSWPH |"
  echo The max used Swap to now is "| $MAXSWPH |"
  echo
  sleep 10

done

