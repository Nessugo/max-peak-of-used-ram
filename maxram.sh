#!/bin/bash

#inizializzo MAXRAM con attuale RAM in uso (MAXRAM<-integer, MAXRAMH<-in GB)
MAXRAM=$(free | grep Mem | awk '{print $3}')
MAXRAMH=$(free -h | grep Mem | awk '{print $3}')

echo
echo "                  |MAX PEAK OF USED RAM|                 "
echo "|Save in the var called MAXRAMH the max peak of used RAM|"
echo " Print the actual RAM and his max peak every 10 seconds"
echo
echo "Press <CTRL+C> to exit."
echo

while :
do
    
  #attuale RAM in uso (TMPRAM<-integer, TMPRAMH<-in GB)
  TMPRAM=$(free | grep Mem | awk '{print $3}')
  TMPRAMH=$(free -h | grep Mem | awk '{print $3}')

      #controllo quale valore è maggiore e lo lascio/metto nella VAR MAXRAM/H
      if (( TMPRAM > MAXRAM )) 
    	then
    	MAXRAM=$TMPRAM
    	MAXRAMH=$TMPRAMH
      fi

  #stampo memoria attuale e memoria di picco in Gb
  echo The actual usage of ram is "| $TMPRAMH |"
  echo The max used RAM to now is "| $MAXRAMH |"
  echo
  sleep 10

done
